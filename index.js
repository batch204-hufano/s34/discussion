// Require directive is use to load the express module/packages.
// It allows us to access the methods and function in easily creating our server.
const express = require ('express');

// This creates an express application and stores this in a constant called app.
// In layman's term, app is our server.
const app = express();

// Setup for allowing the server to handle data from request
// Methods used express JS are middleware
    // Middleware is software that provides common services and capabilities to applications outside of what's offered by the operating system.
app.use(express.json()); // Allows your app to read json data.

// For our application server to run, we need a port to listen
const port = 3000;

// Express has methods corresponding to each HTTP method.
// '/' corresponds with our base URI
// localhost:3000/
// Syntax: app.httpMethod('endpoint', (request,response) => {})
app.get('/', (request, response) => {
    // response.send uses the express JS module's method to send a response back to the client
    response.send('Hello World');
});

// This route expects to received a POST requests at the URI /endpoint '/hello'
app.post('/hello', (request,response) => {
    
    //request.body contains the contents/data of the request.
    console.log(request.body);
    response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});

/* 
    SCENARIO:

		We want to create a simple users database that will perform CRUD operations based on the client request. The following routes should peform its functionality:
*/
    // mock database for our users
    let users = [
        {
            username: "janedoe",
            password: "jane123"
        },
        {
            username: "johnsmith",
            password: "john123"
        }
    ];

    // This route expects to receive a POST request at the URI '/signup'
    // This will create a user object in the 'users' array that mirrors a real world registration:
        // All fields are required/fill out.
    
    app.post('/signup', (request,response) => {

        console.log(request.body);

        // If contents of "request.body" with property of 'username' and 'password' is not empty.
        if(request.body.username !== "" && request.body.password !== "" && request.body.username !== undefined && request.body.password !== undefined) {
            
            // To store the request.body sent via Postman in the 'users' array we will use 'push' method.
            // user object will be saved in our users array.
            users.push(request.body);

            response.send(`User ${request.body.username} succesfully registered!`);
        } 
        // If the username and password are not complete
        else {
            response.send("Please input BOTH username and password.");
        }
    });

    // This route expects to receive a GET request at the URI 'users'
    // This will retrieve all the users stored in the variable created above.
    app.get('/users', (request, response) => {
        response.send(users);
    });

    // This route expects to receive a PUT request at the URI '/change-password'
    // This will update the password of a user that matches the information provided in the client/Postman
        // We will use the 'username' as the search property
    app.put('/change-password', (request,response) => {

        // Creates a variable to store the message to be sent back to the client/Postman(response)
        let message;

        // Create a for loop that will loop through the elements of the 'users' array
        for(let i = 0; i < users.length; i++){

            // If the 'username' provided in the client/Postman and username of the current element/object in the loop is the same.
            if(users[i].username === request.body.username){
                // Changes the password of the users found by the loop into the password provided in the client/postman.

                users[i].password = request.body.password;

                message = `User ${request.body.username}'s password has been updated.`

                // Break out of the once the user that matches the username provided in the client/Postman is found
                break;
            }
            // If no user was found
            else {
                // Changes the message to be sent back as the response.
                message = "Users does not exist!";
            }
        }
        
        // Sends a response back to the client/Postman once the password has been updated or if a user is not found.
        response.send(message);
    });

    // This route expects to receive a DELETE request at the URI "/delete-user"
    // This will remove the user from the array for deletion

    app.delete('/delete-user', (request,response) => {

        // Creates a variable to store the message to be sent back to the client/Postman(response)
        let message;

        /* 
        STRECH GOAL FOR DELETE:
            Create a condition if there are no user existing in the array, send a response "Database is currently empty", else it will proceed in the user deletion if match is found.
        */
        if (users.length === 0){
            message = "Database is currently empty";
        }
        else {
            // Create a for loop that will loop through the elements of the 'users' array
            for(let i = 0; i < users.length; i++){

                // If the 'username' provided in the client/Postman and username of the current element/object in the loop is the same.
                if(users[i].username === request.body.username){

                    // The splice method manipulates the array and removes the user object  from the "users" array based on it's index.
                    users.splice(users[i], 1)

                    message = `User ${request.body.username}'s has been deleted.`

                    // Break out of the once the user that matches the username provided in the client/Postman is found
                    break;
                }
                // If no user was found
                else {
                    // Changes the message to be sent back as the response.
                    message = "Users does not exist!";
                }
            }
        }
        // Sends a response back to the client/Postman once the password has been updated or if a user is not found.
        response.send(message);
    });

// Returns a message to confirm that the server is running in the terminal. 
app.listen(port, () => console.log (`Server is running at port: ${port}`))